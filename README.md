# flaconi_weather

 Overview

- Follows clean code architecture

- Used flutter 2.9 and Null-safety

- Uses flutter bloc for state management

- Have added support for landscape mode as well along with portrait.

- All features are segregated into separate components that can be easily reused wherever possible

- Have not added any test cases as was not required in the assignment although the codebase is structured in a way where we can achieve 100% test coverage.

- Couldn't add the conversion of celsius due to limited time although can be done if requested.


<p float="left">
  <img src="./screenshots/1.png" width="100" />
  <img src="./screenshots/2.png" width="100" />
  <img src="./screenshots/3.png" width="100" />

</p>