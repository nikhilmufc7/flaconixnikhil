import 'package:flaconi_weather/data/weather/models/weather_response.dart';
import 'package:flaconi_weather/domain/weather/repository/weather_repository.dart';

class WeatherUsecase {
  final WeatherRepository _weatherRepository;

  WeatherUsecase(this._weatherRepository);

  Future<WeatherResponse?> getWeatherData() {
    return _weatherRepository.getWeatherResponse();
  }
}
