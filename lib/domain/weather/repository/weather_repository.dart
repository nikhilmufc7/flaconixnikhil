import 'package:flaconi_weather/data/weather/models/weather_response.dart';

abstract class WeatherRepository {
  Future<WeatherResponse?> getWeatherResponse();
}
