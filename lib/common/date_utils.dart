import 'package:intl/intl.dart';

class GetDateUtils {
  String getDayOfTheWeek(String dateTime) {
    return DateFormat('EEEE').format(DateTime.parse(dateTime));
  }
}
