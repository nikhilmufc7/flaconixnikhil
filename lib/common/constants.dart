class Constants {
  static const String cloudyImage =
      "https://images.unsplash.com/photo-1523687996313-ce446f7a658f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NjF8fGNsb3VkeXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=800&q=60";
  static const String defaultImage =
      "https://images.unsplash.com/photo-1590552515252-3a5a1bce7bed?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8d2VhdGhlcnxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=800&q=60";
  static const String rainyImage =
      "https://images.unsplash.com/photo-1567688993206-43c34131b21f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8cmFpbnl8ZW58MHx8MHx8&auto=format&fit=crop&w=800&q=60";
  static const String clearImage =
      "https://images.unsplash.com/photo-1568547741072-0c2468182867?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mzl8fHN1bm55fGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=800&q=60";

  static const String degreeCelsius = "\u2103";
  static const String fahrenheit = "\u2109";

  static const baseUrl = "https://www.metaweather.com";
  static const apiUrl = "/api/location/638242/";
}
