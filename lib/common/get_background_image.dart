import 'package:flaconi_weather/common/constants.dart';

class GetBackgroundImage {
  String getImage(WeatherTypes type) {
    if (type == WeatherTypes.showers) {
      return Constants.rainyImage;
    } else if (type == WeatherTypes.cloud) {
      return Constants.cloudyImage;
    } else if (type == WeatherTypes.clear) {
      return Constants.clearImage;
    } else {
      return Constants.defaultImage;
    }
  }
}

WeatherTypes getWeatherType(String value) {
  if (value.contains("Cloud")) {
    return WeatherTypes.cloud;
  } else if (value.contains("Clear")) {
    return WeatherTypes.clear;
  } else if (value.contains("Showers")) {
    return WeatherTypes.showers;
  } else {
    return WeatherTypes.placeholder;
  }
}

enum WeatherTypes {
  cloud,
  showers,
  clear,
  placeholder,
}
