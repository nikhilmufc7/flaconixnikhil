import 'package:flaconi_weather/presentation/weather/screens/weather_screen.dart';
import 'package:flutter/material.dart';
import 'service_locator.dart' as sl;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await sl.init();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Weather app',
      debugShowCheckedModeBanner: false,
      home: WeatherScreen(),
    );
  }
}
