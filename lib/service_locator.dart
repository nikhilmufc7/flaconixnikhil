import 'package:flaconi_weather/data/weather/endpoints/weather_endpoint.dart';
import 'package:flaconi_weather/data/weather/repository/weather_repo_impl.dart';
import 'package:flaconi_weather/domain/weather/repository/weather_repository.dart';
import 'package:flaconi_weather/domain/weather/usecases/weather_usecase.dart';
import 'package:get_it/get_it.dart';
import 'package:dio/dio.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // Use cases
  sl.registerLazySingleton<WeatherUsecase>(() => WeatherUsecase(sl()));

  // Repository
  sl.registerLazySingleton<WeatherRepository>(
    () => WeatherRepoImplementation(sl()),
  );

  // Data sources
  sl.registerLazySingleton<WeatherEndpoint>(
    () => WeatherEndpoint(sl()),
  );

  //! External
  sl.registerLazySingleton(() => initialiseDefaultDio());
}

Dio initialiseDefaultDio() {
  final dio = Dio();
  return dio;
}
