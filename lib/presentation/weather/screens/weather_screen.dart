import 'package:flaconi_weather/presentation/weather/cubit/weather_cubit.dart';
import 'package:flaconi_weather/presentation/weather/widgets/weather_error_widget.dart';
import 'package:flaconi_weather/presentation/weather/widgets/weather_success_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../service_locator.dart';

class WeatherScreen extends StatefulWidget {
  const WeatherScreen({Key? key}) : super(key: key);

  @override
  State<WeatherScreen> createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {
  final WeatherCubit _weatherCubit = WeatherCubit(sl());

  @override
  void initState() {
    super.initState();
    _weatherCubit.getWeatherData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder(
          bloc: _weatherCubit,
          buildWhen: (_, newStates) {
            return newStates is WeatherLoadingState ||
                newStates is WeatherSuccessState ||
                newStates is WeatherErrorState;
          },
          builder: (context, state) {
            if (state is WeatherSuccessState) {
              return WeatherSuccessWidget(
                weatherResponse: state.weatherResponse,
                refresh: () {
                  _weatherCubit.getWeatherData();
                },
                weatherCubit: _weatherCubit,
              );
            } else if (state is WeatherLoadingState) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is WeatherErrorState) {
              return WeatherErrorWidget(
                onPressed: () {
                  _weatherCubit.getWeatherData();
                },
              );
            } else if (state is WeatherEmptyState) {
              return const Center(
                  child: Text("The weather seems to be empty."));
            }
            return const SizedBox.shrink();
          }),
    );
  }
}
