import 'package:flaconi_weather/data/weather/models/consolidated_weather_response.dart';
import 'package:flaconi_weather/data/weather/models/weather_response.dart';
import 'package:flaconi_weather/domain/weather/usecases/weather_usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WeatherCubit extends Cubit<WeatherState> {
  final WeatherUsecase _weatherUsecase;

  WeatherCubit(this._weatherUsecase) : super(WeatherState.onDefaultState());

  ConsolidatedWeatherResponse? consolidatedWeatherResponse;

  Future updateData(WeatherResponse weatherResponse) async {
    emit(WeatherState.onSuccessState(weatherResponse));
  }

  Future getWeatherData() async {
    emit(WeatherState.onLoadingState());

    try {
      final response = await _weatherUsecase.getWeatherData();

      if (response != null) {
        consolidatedWeatherResponse =
            response.consolidatedWeatherResponse.first;
        emit(WeatherState.onSuccessState(response));
      } else if (response!.consolidatedWeatherResponse.isEmpty) {
        emit(WeatherState.onEmptyState());
      } else {
        emit(WeatherState.onErrorState("Something went wrong!"));
      }
    } catch (e) {
      emit(WeatherState.onErrorState("Something went wrong!"));
    }
  }
}

abstract class WeatherState {
  const WeatherState._();

  factory WeatherState.onDefaultState() => const WeatherInitial._();

  factory WeatherState.onLoadingState() => const WeatherLoadingState._();

  factory WeatherState.onSuccessState(WeatherResponse weatherResponse) =>
      WeatherSuccessState._(weatherResponse: weatherResponse);

  factory WeatherState.onEmptyState() => const WeatherEmptyState._();

  factory WeatherState.onErrorState(String errorMessage) =>
      WeatherErrorState._(errorMessage: errorMessage);
}

class WeatherInitial extends WeatherState {
  const WeatherInitial._() : super._();
}

class WeatherLoadingState extends WeatherState {
  const WeatherLoadingState._() : super._();
}

class WeatherSuccessState extends WeatherState {
  final WeatherResponse? weatherResponse;

  const WeatherSuccessState._({this.weatherResponse}) : super._();
}

class WeatherEmptyState extends WeatherState {
  const WeatherEmptyState._() : super._();
}

class WeatherErrorState extends WeatherState {
  final String? errorMessage;

  const WeatherErrorState._({this.errorMessage}) : super._();
}
