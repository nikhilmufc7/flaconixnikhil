import 'package:flaconi_weather/common/constants.dart';
import 'package:flaconi_weather/common/date_utils.dart';
import 'package:flaconi_weather/common/get_background_image.dart';
import 'package:flaconi_weather/data/weather/models/consolidated_weather_response.dart';
import 'package:flaconi_weather/presentation/weather/widgets/data_type_widget.dart';
import 'package:flutter/material.dart';

class WeatherListCardWidget extends StatelessWidget {
  final ConsolidatedWeatherResponse? consolidatedWeatherResponse;
  const WeatherListCardWidget({Key? key, this.consolidatedWeatherResponse})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: 150,
      margin: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(GetBackgroundImage().getImage(getWeatherType(
                  consolidatedWeatherResponse?.weatherStateName ?? ''))))),
      child: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Text(
            GetDateUtils().getDayOfTheWeek(
                consolidatedWeatherResponse?.applicableDate ?? ''),
            style: const TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          const SizedBox(
            height: 15,
          ),
          DataTypeWidget(
              type: "High",
              value:
                  "${consolidatedWeatherResponse?.maxTemp.toStringAsFixed(0) ?? ''}${Constants.fahrenheit}"),
          DataTypeWidget(
              type: "Low",
              value:
                  "${consolidatedWeatherResponse?.minTemp.toStringAsFixed(0) ?? ''}${Constants.degreeCelsius}"),
        ],
      ),
    );
  }
}
