import 'package:flaconi_weather/data/weather/models/consolidated_weather_response.dart';
import 'package:flaconi_weather/data/weather/models/weather_response.dart';
import 'package:flaconi_weather/presentation/weather/cubit/weather_cubit.dart';
import 'package:flaconi_weather/presentation/weather/widgets/weather_list_card_widget.dart';
import 'package:flutter/material.dart';

class WeatherListWidget extends StatelessWidget {
  final List<ConsolidatedWeatherResponse>? consolidatedWeatherResponse;
  final WeatherCubit? weatherCubit;
  final WeatherResponse? weatherResponse;
  const WeatherListWidget(
      {Key? key,
      this.consolidatedWeatherResponse,
      this.weatherCubit,
      this.weatherResponse})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 4,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              weatherCubit?.consolidatedWeatherResponse =
                  consolidatedWeatherResponse?[index];
              weatherCubit?.updateData(weatherResponse!);
            },
            child: WeatherListCardWidget(
              consolidatedWeatherResponse: consolidatedWeatherResponse?[index],
            ),
          );
        });
  }
}
