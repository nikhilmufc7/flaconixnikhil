import 'package:flutter/material.dart';

class DataTypeWidget extends StatelessWidget {
  final String? type;
  final String? value;
  const DataTypeWidget({Key? key, this.type, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Text(
        "$type : $value",
        style: const TextStyle(
            fontSize: 16, fontWeight: FontWeight.w600, color: Colors.white),
      ),
    );
  }
}
