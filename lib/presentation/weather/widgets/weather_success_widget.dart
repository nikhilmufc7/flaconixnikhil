import 'package:flaconi_weather/common/date_utils.dart';
import 'package:flaconi_weather/common/get_background_image.dart';
import 'package:flaconi_weather/data/weather/models/weather_response.dart';
import 'package:flaconi_weather/presentation/weather/cubit/weather_cubit.dart';
import 'package:flaconi_weather/presentation/weather/widgets/data_type_widget.dart';
import 'package:flaconi_weather/presentation/weather/widgets/weather_list_widget.dart';
import 'package:flutter/material.dart';

class WeatherSuccessWidget extends StatelessWidget {
  final WeatherResponse? weatherResponse;
  final Function? refresh;
  final WeatherCubit? weatherCubit;
  const WeatherSuccessWidget(
      {Key? key, this.weatherResponse, this.refresh, this.weatherCubit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
          image: DecorationImage(
        fit: BoxFit.cover,
        image: NetworkImage(GetBackgroundImage().getImage(getWeatherType(
            weatherCubit?.consolidatedWeatherResponse?.weatherStateName ??
                ''))),
      )),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: RefreshIndicator(
          onRefresh: refreshData,
          child: ListView(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 40,
                  ),
                  Center(
                    child: Text(
                      GetDateUtils().getDayOfTheWeek(weatherCubit
                              ?.consolidatedWeatherResponse?.applicableDate ??
                          ''),
                      style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: Text(
                      "${weatherResponse?.consolidatedWeatherResponse.first.theTemp.toStringAsFixed(0) ?? ''}\u2103", //\u2109 for F
                      style: const TextStyle(
                          fontSize: 44,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        weatherCubit?.consolidatedWeatherResponse
                                ?.weatherStateName ??
                            '',
                        style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                      Text(
                        weatherResponse?.title ?? '',
                        style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  DataTypeWidget(
                      type: "Humidity",
                      value:
                          "${weatherCubit?.consolidatedWeatherResponse?.humidity.toStringAsFixed(0) ?? ''}%"),
                  DataTypeWidget(
                      type: "Pressure",
                      value:
                          "${weatherCubit?.consolidatedWeatherResponse?.airPressure.toStringAsFixed(0) ?? ''}  hPa"),
                  DataTypeWidget(
                      type: "Wind",
                      value:
                          "${weatherCubit?.consolidatedWeatherResponse?.humidity.toStringAsFixed(0) ?? ''} km/h"),
                  const SizedBox(
                    height: 50,
                  ),
                  SizedBox(
                    height: 150,
                    child: WeatherListWidget(
                      consolidatedWeatherResponse:
                          weatherResponse?.consolidatedWeatherResponse,
                      weatherCubit: weatherCubit,
                      weatherResponse: weatherResponse,
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> refreshData() async {
    refresh!();

    return;
  }
}
