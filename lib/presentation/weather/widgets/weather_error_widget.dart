import 'package:flutter/material.dart';

class WeatherErrorWidget extends StatelessWidget {
  final Function? onPressed;
  const WeatherErrorWidget({Key? key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Text("An error occurred. Please try again"),
        MaterialButton(
          onPressed: () {
            onPressed!();
          },
          child: const Text('Retry'),
        )
      ],
    );
  }
}
