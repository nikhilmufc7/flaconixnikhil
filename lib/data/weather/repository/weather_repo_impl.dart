import 'package:flaconi_weather/data/weather/endpoints/weather_endpoint.dart';
import 'package:flaconi_weather/data/weather/models/weather_response.dart';
import 'package:flaconi_weather/domain/weather/repository/weather_repository.dart';

class WeatherRepoImplementation extends WeatherRepository {
  final WeatherEndpoint _weatherEndpoint;

  WeatherRepoImplementation(this._weatherEndpoint);

  @override
  Future<WeatherResponse?> getWeatherResponse() async {
    final response = await _weatherEndpoint.weatherResponse();
    return response;
  }
}
