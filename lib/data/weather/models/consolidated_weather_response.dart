import 'package:json_annotation/json_annotation.dart';

part 'consolidated_weather_response.g.dart';

@JsonSerializable()
class ConsolidatedWeatherResponse {
  @JsonKey(name: "id")
  int id;

  @JsonKey(name: "weather_state_name")
  String weatherStateName;

  @JsonKey(name: "weather_state_abbr")
  String weatherStateAbbr;

  @JsonKey(name: "wind_direction_compass")
  String windDirectionCompass;

  @JsonKey(name: "created")
  String created;

  @JsonKey(name: "applicable_date")
  String applicableDate;

  @JsonKey(name: "min_temp")
  num minTemp;

  @JsonKey(name: "max_temp")
  num maxTemp;

  @JsonKey(name: "the_temp")
  num theTemp;

  @JsonKey(name: "wind_speed")
  num windSpeed;

  @JsonKey(name: "wind_direction")
  num windDirection;

  @JsonKey(name: "air_pressure")
  num airPressure;

  @JsonKey(name: "humidity")
  num humidity;

  @JsonKey(name: "visibility")
  num visibility;

  @JsonKey(name: "predictability")
  num predictability;

  factory ConsolidatedWeatherResponse.fromJson(Map<String, dynamic> json) =>
      _$ConsolidatedWeatherResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ConsolidatedWeatherResponseToJson(this);

  ConsolidatedWeatherResponse(
    this.applicableDate,
    this.created,
    this.weatherStateAbbr,
    this.weatherStateName,
    this.windDirectionCompass,
    this.id,
    this.minTemp,
    this.maxTemp,
    this.theTemp,
    this.windSpeed,
    this.visibility,
    this.airPressure,
    this.humidity,
    this.predictability,
    this.windDirection,
  );
}
