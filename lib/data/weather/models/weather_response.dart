import 'package:flaconi_weather/data/weather/models/consolidated_weather_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather_response.g.dart';

@JsonSerializable()
class WeatherResponse {
  @JsonKey(name: "consolidated_weather")
  List<ConsolidatedWeatherResponse> consolidatedWeatherResponse;

  @JsonKey(name: "time")
  String? time;

  @JsonKey(name: "sun_rise")
  String? sunRise;

  @JsonKey(name: "sun_set")
  String? sunSet;

  @JsonKey(name: "timezone_name")
  String? timezoneName;

  @JsonKey(name: "title")
  String? title;

  @JsonKey(name: "location_type")
  String? locationType;

  @JsonKey(name: "latt_long")
  String? lattLong;

  @JsonKey(name: "timezone")
  String? timezone;

  factory WeatherResponse.fromJson(Map<String, dynamic> json) =>
      _$WeatherResponseFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherResponseToJson(this);

  WeatherResponse(
      this.consolidatedWeatherResponse,
      this.locationType,
      this.lattLong,
      this.sunRise,
      this.sunSet,
      this.time,
      this.timezone,
      this.timezoneName,
      this.title);
}
