// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherResponse _$WeatherResponseFromJson(Map<String, dynamic> json) =>
    WeatherResponse(
      (json['consolidated_weather'] as List<dynamic>)
          .map((e) =>
              ConsolidatedWeatherResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['location_type'] as String?,
      json['latt_long'] as String?,
      json['sun_rise'] as String?,
      json['sun_set'] as String?,
      json['time'] as String?,
      json['timezone'] as String?,
      json['timezone_name'] as String?,
      json['title'] as String?,
    );

Map<String, dynamic> _$WeatherResponseToJson(WeatherResponse instance) =>
    <String, dynamic>{
      'consolidated_weather': instance.consolidatedWeatherResponse,
      'time': instance.time,
      'sun_rise': instance.sunRise,
      'sun_set': instance.sunSet,
      'timezone_name': instance.timezoneName,
      'title': instance.title,
      'location_type': instance.locationType,
      'latt_long': instance.lattLong,
      'timezone': instance.timezone,
    };
