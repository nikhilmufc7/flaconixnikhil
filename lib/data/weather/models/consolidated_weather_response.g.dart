// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'consolidated_weather_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConsolidatedWeatherResponse _$ConsolidatedWeatherResponseFromJson(
        Map<String, dynamic> json) =>
    ConsolidatedWeatherResponse(
      json['applicable_date'] as String,
      json['created'] as String,
      json['weather_state_abbr'] as String,
      json['weather_state_name'] as String,
      json['wind_direction_compass'] as String,
      json['id'] as int,
      json['min_temp'] as num,
      json['max_temp'] as num,
      json['the_temp'] as num,
      json['wind_speed'] as num,
      json['visibility'] as num,
      json['air_pressure'] as num,
      json['humidity'] as num,
      json['predictability'] as num,
      json['wind_direction'] as num,
    );

Map<String, dynamic> _$ConsolidatedWeatherResponseToJson(
        ConsolidatedWeatherResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'weather_state_name': instance.weatherStateName,
      'weather_state_abbr': instance.weatherStateAbbr,
      'windDirectionCompass': instance.windDirectionCompass,
      'created': instance.created,
      'applicable_date': instance.applicableDate,
      'min_temp': instance.minTemp,
      'max_temp': instance.maxTemp,
      'the_temp': instance.theTemp,
      'wind_speed': instance.windSpeed,
      'wind_direction': instance.windDirection,
      'air_pressure': instance.airPressure,
      'humidity': instance.humidity,
      'visibility': instance.visibility,
      'predictability': instance.predictability,
    };
