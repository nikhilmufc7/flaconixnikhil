import 'package:dio/dio.dart';
import 'package:flaconi_weather/common/constants.dart';
import 'package:flaconi_weather/data/weather/models/weather_response.dart';
import 'package:retrofit/retrofit.dart';

part 'weather_endpoint.g.dart';

@RestApi(baseUrl: Constants.baseUrl)
abstract class WeatherEndpoint {
  factory WeatherEndpoint(Dio dio) = _WeatherEndpoint;

  @GET(Constants.apiUrl)
  Future<WeatherResponse>? weatherResponse();
}
